#!/bin/bash
IMAGE=crawler-from-scratch

docker build -t $IMAGE .

npm set progress=false && \
  npm i && \
  docker run \
    -p 8888:8888 \
    -v $(pwd)/node_modules:/node_modules \
    -v $(pwd)/notebooks:/opt/notebooks \
    --rm -it $IMAGE
