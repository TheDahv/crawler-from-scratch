FROM alekzonder/puppeteer as base

USER root

# ipython ipython-notebook and jp-babel kernel setup
RUN apt update && apt -y install python2.7 python-pip python-dev
RUN pip install --upgrade pip
RUN pip install jupyter
RUN yarn global add jp-babel
RUN /usr/local/share/.config/yarn/global/node_modules/.bin/jp-babel-install --install=global

# Run everything after as non-privileged user to make puppeteer happy
USER pptruser

# The actual notebook project-specific code
WORKDIR /app
COPY ./package.json ./package-lock.json ./
RUN npm install
ENV NODE_PATH=/node_modules:/usr/local/share/.config/yarn/global/node_modules:$NODE_PATH
CMD jupyter notebook --notebook-dir=/opt/notebooks --ip='*' --port=8888 --no-browser --allow-root
