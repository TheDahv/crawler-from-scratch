# Building a Crawler from Scratch

Welcome, and thanks for sharing your time with me to learn about crawlers.

This repository contains the Docker images and scripts to get an interactive
notebook envionment up and running with all you need to try it out.

There are [some
slides](https://docs.google.com/presentation/d/1qVjaELJ-W9Ihsruk_B1ndYGu5v1lTFyvJcKJZT6K11o/edit?usp=sharing)
you may want to read through first if you don't have any background in crawlers.

## Installing

First, use `git` to check out this project to your local computer:

```sh
git clone git@gitlab.com:TheDahv/crawler-from-scratch.git
cd crawler-from-scratch
```

This project needs a few things on your computer to run:

- [Docker](https://docs.docker.com/install/)
- [Node.js](https://nodejs.org/en/) - version 8 or greater

## Getting Started

Find `build-and-run.sh` in the folder you used to clone the project. Run it like
this to build the image, download dependencies, and launch the notebook:

```sh
./build-and-run.sh
```

You should see a lot of output go by. At the end, the Jupyter notebook
server will tell you what URL to open to access the notebook. Your token will
look different on your computer, but it should look like this:

```
[I 22:32:37.221 NotebookApp] Writing notebook server cookie secret to
/home/pptruser/.local/share/jupyter/runtime/notebook_cookie_secret
[W 22:32:37.481 NotebookApp] WARNING: The notebook server is listening on all IP
addresses and not using encryption. This is not recommended.
[I 22:32:37.497 NotebookApp] Serving notebooks from local directory:
/opt/notebooks
[I 22:32:37.497 NotebookApp] The Jupyter Notebook is running at:
[I 22:32:37.497 NotebookApp] http://(877950246c4a or
127.0.0.1):8888/?token=7a4767800c67b3cd049b4599b8f1db26bf291e7b29bfcc8d
[I 22:32:37.497 NotebookApp] Use Control-C to stop this server and shut down all
kernels (twice to skip confirmation).
[C 22:32:37.498 NotebookApp]

    Copy/paste this URL into your browser when you connect for the first time,
    to login with a token:

        http://(877950246c4a or 127.0.0.1):8888/?token=7a4767800c67b3cd049b4599b8f1db26bf291e7b29bfcc8d
```

That last URL is what you want. The notebook is running in a container, but you
want to access it from your laptop. Copy and paste everything from
`:8888/?token` to the end of the line.

Then open a browser and type into the URL bar:

```
http://localhost
```

Paste in the port and token after so you end up with a URL that looks like

```
http://localhost:8888/?token=7a4767800c67b3cd049b4599b8f1db26bf291e7b29bfcc8d
```

**REMEMBER** your token won't be the same as what is in this documentation.

## Interacting with the Notebook

Once you load the notebook in the browser, you should see one entry in the table
of contents. Select `Crawler From Scratch.ipynb` to get started.

Notice the notebook is made up of cells with either text or code.

You can click on a cell you want to run and click the Run button in the top
menu bar. Otherwise, you can press `shift + enter` to execute a cell.

Its results should print below the cell's contents.

## Different Examples

The main branch contains a blank slate for you to work with. Howerver, there are
other branches with other completed examples for you to study and play with.

You can find all branches
[here](https://gitlab.com/TheDahv/crawler-from-scratch/branches). You can also
learn about them here:

- [`crawler-simple-final`](https://gitlab.com/TheDahv/crawler-from-scratch/tree/crawler-simple-final):
  implements two simple crawlers using a procedural and a streaming style
- [`crawler-puppeteer`](https://gitlab.com/TheDahv/crawler-from-scratch/tree/crawler-puppeteer):
  implements a crawler using Google's
  [Puppeteer](https://developers.google.com/web/tools/puppeteer/) to deal with
  asynchronous HTML pages
